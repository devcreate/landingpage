function initSignInValidate(){

  $('#sign-in__form').validate({
    submitHandler: function() {
      let signInEmail = $('#sign-in__email').val();
      console.log(signInEmail);
      alert("Thanks. The application is accepted. Our manager will contact you shortly.");
      signInEmail.reset();
    },
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: "Please enter a valid email address"
    }
  });
}

$(document).ready(function(){
  initSignInValidate();
});
