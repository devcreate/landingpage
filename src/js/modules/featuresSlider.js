// import Swiper bundle with all modules installed
import Swiper from 'swiper/bundle';


const swiper = new Swiper('.swiper-container', {
  // Optional parameters
  spaceBetween: 0,
  slidesPerView: 5,
  loop: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
    },
    490: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 3,
    },
    1024: {
      slidesPerView: 5,
    }
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

});

