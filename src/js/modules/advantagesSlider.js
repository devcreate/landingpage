// import Swiper bundle with all modules installed
import Swiper from 'swiper/bundle';


const swiper = new Swiper('.swiper-container-advantages', {
  // Optional parameters
  spaceBetween: 0,
  slidesPerView: 3,
  loop: false,
  pagination: {
    el: '.s-advantages__pagination',
    clickable: true,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    767: {
      slidesPerView: 1,
    },
    768: {
      slidesPerView: 2,
    },
    1024: {
      slidesPerView: 3,
    },
  },
});
