import './modules/sitePreloader';
import documentReady from './utils/documentReady';
import documentLoaded from './utils/documentLoaded';
import jqueryValidate from './utils/jqueryValidate';

import cssVars from './modules/cssVars';
import resize from './modules/resize';
import lazyload from './modules/lazyload';
import featuresSlider from './modules/featuresSlider';
import advantagesSlider from './modules/advantagesSlider';
import reviewsSlider from './modules/reviewsSlider';
import scrollTo from './modules/scrollTo';
import validate from './modules/validate';




documentReady(() => {
  cssVars.init();
  resize.init();
  lazyload.init();
  scrollTo.init();
  jqueryValidate.init();

});

documentLoaded(() => {

});
